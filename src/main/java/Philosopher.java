public class Philosopher implements Runnable {
    private Object leftFork;
    private Object rightFork;
    private int rice = 10;

    public Philosopher(Object leftFork, Object rightFork) {
        this.leftFork = leftFork;
        this.rightFork = rightFork;
    }

    public void doAction(String action) throws InterruptedException {
        System.out.println(
                Thread.currentThread().getName() + " " + action);
        Thread.sleep(5);

    }


    @Override
    public void run() {
        try {
            while (rice > 0) {
                doAction(
                        "Rice : " + this.rice + "   : Thinking");
                synchronized (leftFork) {
                    doAction(
                            "Rice : " + this.rice +
                                    " : Picked up left fork");
                    synchronized (rightFork) {
                        doAction(
                                "Rice: " + this.rice +
                                        " : Picked up right fork and started eating");
                        doAction(
                                "Rice : " + this.rice +
                                        " : Put down right fork");
                        this.rice -= 1;
                    }
                    doAction(
                            "Rice : " + this.rice +
                                    " : Put down left fork and start thinking");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return;
        }
    }
}